/**
 * Created by richard on 19/10/16.
 */

/*global angular */
'use strict';

//noinspection JSUnusedGlobalSymbols
angular.module('bitcraft-db-edition-select')
    .component('bitcraftDbEditionSelect', {
        templateUrl: './js/db-edition-select/db-edition-select.template.html', // this line will be replaced
        bindings: {
            value: '=',
            values: '<',
            edit: '<'
        }
    });
